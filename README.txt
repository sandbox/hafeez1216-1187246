
CKEDITOR Tooltips - A PLUGIN TO EASILY create tooltips in ckeditor
Project link will be coming soon

This module is an extension to the wysiwyg module when ckeditor is used as editor.

As of now it only supports CKEditor installed through the Wysiwyg module.

This module currently allows users to easily create tooltips with the help of tooltips contributed module. 
The problem with tooltips module is that user needs to be aware of the format in order to create a tooltip. 
This module just provides an option(button) in the ckeditor toolbar, so users just need to click on it which will open a dialog box 
prompting users to provide the text that needs to be highlighted and the text or image to be shown when the mouse is hover over the 
highlighted text.

-- REQUIREMENTS --

Currently, CKEditor Tooltips needs the following to run:

Wysiwyg module, version 6.x-2.1 or above
the CKEditor editor, version 3.5.3 or above.


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.

Copy the ckeditor_tooltips folder to your sites/all/modules directory.
Navigate to admin/build/modules and enable the module.

Navigate to admin/settings/wysiwyg and 


-- CONFIGURATION --

* Go to Administer > Site configuration > Wysiwyg profiles and

* Edit CKEditor profile (click on the Edit button next to the input format CKEditor is assigned to).

* Under Buttons and plugins, check tooltips checkbox.

* Save changes.  

-- CONTACT --

Current maintainer:
* Abdul Hafeez (rightchoice2c_me) - http://drupal.org/user/315349
