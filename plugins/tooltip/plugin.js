/**
 * Ckeditor plugin to insert tooltips.
 */

CKEDITOR.plugins.add( 'tooltip',
{
	
	init: function( editor )
	{
		regexGetSizeOrEmpty = /(^\s*(\d+)((px)|\%)?\s*$)|^$/i;	
			
		var checkIfImgExists =  function(str, tag) {		
		return new RegExp('<'+tag+'[^>]*>','g').test(str);
		};
		
		// Handles the event when the "Type" selection box is changed.
		var tooltipChanged = function()
		{
		
			var dialog = this.getDialog(),
				partIds = [ 'textOptions', 'imageOptions'],
				typeValue = this.getValue();	
				modifyToolTip(dialog, partIds, typeValue);
			
		};
		
		var modifyToolTip = function(dialog, partIds, typeValue) {
			dialog.showPage( 'tab1' );
			if ( typeValue == 'image' )
			{
				dialog.showPage( 'tab2' );
				
			} else {
				dialog.hidePage( 'tab2' );
			}
			for ( var i = 0 ; i < partIds.length ; i++ )
			{
				var element = dialog.getContentElement( 'tab1', partIds[i] );
				if ( !element )
					continue;

				element = element.getElement().getParent().getParent();
				if ( partIds[i] == typeValue + 'Options' )
					element.show();
				else
					element.hide();
			}	
			dialog.layout();			
		};
		
		var isValidURL =  function(url){
			var RegExp = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
			if (RegExp.test(url)) {
				return true;
			} else {
				return false;
			}
		};

		editor.addCommand( 'tooltipDialog',new CKEDITOR.dialogCommand( 'tooltipDialog' ) );
		editor.ui.addButton( 'tooltip',
		{
			label: 'Click to create Tooltip',
			command: 'tooltipDialog',
			icon: this.path + 'images/image.jpg'
		} );
		CKEDITOR.dialog.add( 'tooltipDialog', function ( editor )
		{
			linkLang = editor.lang.link;	
			
			return {
				title : 'Tooltip Properties',
				minWidth : 400,
				minHeight : 200,
				contents :
				[
					{
						id : 'tab1',
						label : 'Basic Settings',
						elements :
						[
							{
								type : 'text',
								id : 'highlightedText',
								label : 'Text to highlight',
								validate : CKEDITOR.dialog.validate.notEmpty( "Text to highlight field cannot be empty" )
							},
							{
								id : 'linkType',
								type : 'select',
								label : 'Tooltip Type',
								'default' : 'text',
								items :
								[
									[ 'Text', 'text' ],
									[ 'Image', 'image' ],									
								],
								onChange : tooltipChanged								
							},							
							{
								type :  'vbox',
								id : 'textOptions',
								padding : 1,
								children :
								[
									{
										type : 'text',
										id : 'tooltipText',
										label : 'Tooltip Text',										
										validate : function()
										{
											var dialog = this.getDialog();

											if ( !dialog.getContentElement( 'tab1', 'linkType' ) ||
													dialog.getValueOf( 'tab1', 'linkType' ) != 'text' )
												return true;

											var func = CKEDITOR.dialog.validate.notEmpty( 'Tooltip Text cannot be empty' );
											return func.apply( this );
										}										
									}									
								]
							},
							{
								type :  'vbox',
								id : 'imageOptions',
								padding : 1,
								children :
								[
									{
										type : 'text',
										id : 'tooltipImage',
										label : 'Image Url',										
										validate : function()
										{
											var dialog = this.getDialog();

											if ( !dialog.getContentElement( 'tab1', 'linkType' ) ||
													dialog.getValueOf( 'tab1', 'linkType' ) != 'image' )
												return true;
											var aMatch  =  isValidURL(this.getValue());
											if ( !aMatch )
												alert( 'Tooltip Image Url should be a valid url'  );
												return !!aMatch;											
										}										
									}									
								]								
							}							
						]
					},
					{
						id : 'tab2',
						label : 'Advanced Settings',
						elements :
						[
							{
								type : 'text',
								id : 'imgWidth',
								label : 'Width',
								validate : function()
								{
									var dialog = this.getDialog();

									if ( !dialog.getContentElement( 'tab1', 'linkType' ) ||
											dialog.getValueOf( 'tab1', 'linkType' ) != 'image' )
										return true;								   								
									var aMatch  =  this.getValue().match( regexGetSizeOrEmpty );
									if ( !aMatch )
										alert( editor.lang.common.invalidWidth );
									return !!aMatch;
								}
							},
							{
								type : 'text',
								id : 'imgHeight',
								label : 'Height',
								validate : function()
								{
									var dialog = this.getDialog();

									if ( !dialog.getContentElement( 'tab1', 'linkType' ) ||
											dialog.getValueOf( 'tab1', 'linkType' ) != 'image' )
										return true;
									var aMatch  =  this.getValue().match( regexGetSizeOrEmpty );
									if ( !aMatch )
										alert( editor.lang.common.invalidHeight );
									return !!aMatch;									
								}
							}
						]
					}
				],
				onOk : function()
				{
					var dialog = this;
					var editor = this.getParentEditor();
					var selection = editor.getSelection();										
					var div = editor.document.createElement( 'span' );	
					div.setAttribute('class', 'cke-element-type-tooltip');					
					div.appendHtml('[tip: ');
					div.appendHtml(dialog.getValueOf( 'tab1', 'highlightedText' ));
					div.appendHtml(' = ');	
					if (dialog.getValueOf( 'tab1', 'linkType' ) == "image") {
						var width = '';
						var height = '';
						var src = dialog.getValueOf( 'tab1', 'tooltipImage' );
						if (dialog.getValueOf( 'tab2', 'imgWidth' ) > 0) {
							width = dialog.getValueOf( 'tab2', 'imgWidth' );
						}
						if (dialog.getValueOf( 'tab2', 'imgHeight' ) > 0) {
							height = dialog.getValueOf( 'tab2', 'imgHeight' );
						}					
						
						//var image = '<img src="'+src+'" '+width+ ' '+height+' />' ;		
						var image = editor.document.createElement( 'img' );
							image.setAttribute( 'src', src );	
							if (width > 0) 
							image.setAttribute( 'width', width );						
							if (height > 0) 
							image.setAttribute( 'height', height );
							div.append(image);
					} else {
						text = dialog.getValueOf( 'tab1', 'tooltipText');
						div.appendHtml(text);
					}									
					
					div.appendHtml(' ]');					
					editor.insertElement( div );
					selection.selectElement( div );
				},
				// Inital focus on 'url' field if link is of type URL.
				onFocus : function()
				{			
					var dialog = this;
					var linkType = dialog.getValueOf( 'tab1', 'linkType' );					
					partIds = [ 'textOptions', 'imageOptions'],					
					modifyToolTip(dialog, partIds, linkType);
					
				},
				onShow : function()
				{					
					var dialog = this;
					var editor = this.getParentEditor(),
						selection = editor.getSelection(),
						element = null;
						
					if (editor.getSelection().getStartElement().getOuterHtml() != '') {
						element = editor.getSelection().getStartElement();							
						if (element.hasAttribute('class') && element.getAttribute('class') == "cke-element-type-tooltip") {
							selection.selectElement( element );
							var html = element.getHtml();
							var matches = html.match(/\[(.*?)\]/);	
							if (matches) {
								var tooltip = matches[1];
								var tipText = tooltip.substring(5);
								var i = tipText.indexOf('=');										
								var tooltipHText = tipText.slice(0,i);	
								var tooltipText = tipText.slice(i+1);										
								if (tooltipHText != '') {
									dialog.setValueOf( 'tab1', 'highlightedText',  tooltipHText);
								}	
								if (tooltipText != '') {
									tooltipText = tooltipText.toString().toLowerCase();
									var chkImg = checkIfImgExists(tooltipText, 'img');										
									if (!chkImg) {
										dialog.setValueOf( 'tab1', 'tooltipText',  tooltipText);
									} else {		
										image = tooltipText;																				
										var imgSrc = $(image).attr('src');
										var imgWidth = $(image).attr('width');										
										var imgHeight = $(image).attr('height');
										dialog.setValueOf( 'tab1', 'linkType',  'image');
										if (imgSrc != '' && imgSrc != undefined) {											
											dialog.setValueOf( 'tab1', 'tooltipImage',  imgSrc);
										}
										if (imgWidth != '' && imgWidth != undefined) {											
											dialog.setValueOf( 'tab2', 'imgWidth',  imgWidth);
										}
										if (imgHeight != '' && imgHeight != undefined) {											
											dialog.setValueOf( 'tab2', 'imgHeight',  imgHeight);
										}										
										dialog.showPage( 'tab2' );
									}
								}									
							}
						}
					}
				}
			};
		} );
	}
} );